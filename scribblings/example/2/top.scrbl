#lang scribble/manual

@require[scribble-include-text]

@(define var "Blah")

@title{What a wonderful compliment}

@include-text{charmer.scrbl}

I doubt you really mean that.

@include-text{blah.scrbl}

No more, you're frightening me.

