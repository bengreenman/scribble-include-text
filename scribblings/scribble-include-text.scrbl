#lang scribble/manual

@require[
  scribble/example
  (for-label racket/base
             (only-in racket/contract/base -> any/c)
             scribble-include-text)]

@title{Scribble: include text}

@defmodule[scribble-include-text]{
  Put the full contents of one document into another.
}

@defform[(include-text filename)]{
  Read the contents of the file @racket[filename] into a syntax object and inject
  it into the current Scribble document.
}

Example:

@bold{side.scrbl}
@codeblock|{
This file has some text,
and calls a @emph{Scribble} function,
and references a @|variable|.
}|

@bold{top.scrbl}
@codeblock|{
#lang scribble/manual
@require[scribble-include-text]

@title{Example}

@(define variable "cat")

@include-text{side.scrbl}

The end.
}|

The section below is the rendered output.

@include-section{example/1/top.scrbl}

