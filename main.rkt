#lang racket/base

(provide
  include-text)

(require
  racket/include
  (for-syntax racket/base syntax/parse (only-in scribble/reader make-at-reader)))

;; ---

(define-syntax (include-text stx)
  (syntax-parse stx
   ((_ mod:string)
    (with-syntax ((ctx stx))
      (syntax-local-introduce
        (syntax/loc stx
          (include-at/relative-to/reader ctx ctx mod wrap-at-reader)))))))

(define-for-syntax wrap-at-reader
  ;; similar to make-at-reader, but with output that fits include/reader
  (let ((reader (make-at-reader #:inside? #t)))
    (lambda (src in-port)
      (if (eof-object? (peek-byte in-port))
        eof
        (cons #'begin (reader src in-port))))))

